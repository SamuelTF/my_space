<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', 'UserController@list');

Route::get('/edit', function () {
    return view('edit');
});

Route::get('profile', 'UserController@profile');
Route::post('profile', 'UserController@update_profile');

Route::Post('/search', 'UserController@search_profile');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('user/{id}', 'UserController@show')->name('user');

Route::get('like', 'UserController@profile');
Route::get('like/{id}', 'LikeController@update_likes')->name('like');
