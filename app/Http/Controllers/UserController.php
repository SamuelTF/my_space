<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Auth;
use Image;

class UserController extends Controller
{
    //
    public function profile()
    {
      return view('profile', array('user' => Auth::user(), 'likes' => Likes::get()) );
    }

    public function update_profile(Request $req)
    {

    	if($req->hasFile('avatar') | !empty($req->input('address')) | !empty($req->input('relation_status'))){
        // Handle the user upload of avatar
    		$avatar = $req->file('avatar');
    		$filename = time() . '.' . $avatar->getClientOriginalExtension();
    		Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' . $filename ) );

        // Handle the address upload
        $address = $req->input('address');

        // Handle the relation_status upload
        $relation_status = $req->input('relation_status');

    		$user = Auth::user();
    		$user->avatar = $filename;
        $user->address = $address;
        $user->relation_status = $relation_status;
    		$user->save();
    	}

    	return view('profile', array('user' => Auth::user()) );

    }

    public function search_profile($req)
    {
      $search_input = $req->input('search_input');

      if (!empty($search_input)) {
        $search_outcome = User::where('name', '=', $search_input);

        if ($search_outcome > 0) {
          return view('search', ['search_input' => $search_outcome]);
        }
        return view('search')->withMessage('No users found!');
      }
    }

    public function show(Request $req, $id)
    {
      $user = User::findOrFail($id);

      return view('profile')->with('user', $user);
    }

    public function list(Request $req)
    {
      $users = User::get();

      return view('home', ['users' => $users]);
    }
}
