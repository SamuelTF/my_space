<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Likes;
use Auth;

class LikeController extends Controller
{

    public function update_likes(Request $req , $id)
    {
        if (Likes::where('liked_by_user_id', Auth::id())->first()) {

          Likes::where('liked_by_user_id', Auth::id())->delete();

        } else {

          $likes = new Likes;
          $likes->liked_user_id = $id;
          $likes->liked_by_user_id = Auth::id();
          $likes->save();


        }

        $likesCount = Likes::where('liked_user_id', $id)->count();

        return redirect()->back()->with('data', ['data']); 
    }
}
