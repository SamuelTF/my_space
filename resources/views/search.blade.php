@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Users</h1></div>
                @if(isset($search_outcome))
            			<h2>Sample User details</h2>
            			<table class="table table-striped">
            				<thead>
            					<tr>
            						<th>Name</th>
            					</tr>
            				</thead>
            				<tbody>
            					@foreach($search_outcome as $user)
              					<tr>
              						<td>{{$user->name}}</td>
              					</tr>
            					@endforeach
            				</tbody>
            			</table>
            	   @elseif(isset($message))
            			<p>{{ $message }}</p>
          			@endif
            </div>
        </div>
    </div>
</div>
@endsection
