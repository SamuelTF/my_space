@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit profile</div>
                  @guest
                    <h2>Please login</h2>
                  @else
                    <div class="card-body">
                      <form enctype="multipart/form-data" action="/profile" method="POST">

                          <label>Update Profile Image</label>
                          <input type="file" name="avatar">

                          <input type="hidden" name="_token" value="{{ csrf_token() }}">

                          <label>Updata Address</label>
                          <input type="text" name="address">

                          <label>Updata Relation Status</label>
                          <input type="text" name="relation_status">

                          <input type="submit" class="left btn btn-sm btn-primary">
                      </form>
                    </div>
                  @endguest
            </div>
        </div>
    </div>
</div>
@endsection
