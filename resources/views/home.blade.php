@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Users</h1></div>
                <div class="card-body">
                  <div class="card">
                    @foreach ($users as $user)
                      <a href="/user/{{$user->id}}">
                        <div class="card-header text-center bg-dark text-light"><h2>{{ $user->name }}</h2></div>
                      </a>
                    @endforeach
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
