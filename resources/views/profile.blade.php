@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
          @guest
            <img src="/uploads/avatars/{{ $user->avatar }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
            <h2>Test</h2> <br>
          @else
            <img src="/uploads/avatars/{{ $user->avatar }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
            <h2>{{ $user->name }}</h2>
            <!-- @if (session()->has('data'))
              <p class="text-primary">Likes: {{session('data')}}</p>
            @endif   -->
            <br>
            <p>{{ $user->address }}</p>
            <p>{{ $user->relation_status }}</p>
            <br>

            <a class="btn btn-sm btn-primary" href="{{ route('like', ['id' => $user->id])}}">Like</a>

          @endguest
        </div>
    </div>
</div>
@endsection
